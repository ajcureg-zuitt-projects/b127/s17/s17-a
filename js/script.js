// Key Concepts
// push()
// indexOf()
// if()
// addName()


// Activity:
// let tuitt = [
// 		"Charles", "Paul", "Sef", "Alex"];

// addName('Charles', tuitt)
// // Already exists

// addName('Alan', tuitt)
// // Alan successfully added


let tuitt = [
		"Charles", "Paul", "Sef", "Alex"];

function addName(name, tuitt){
	if (tuitt.indexOf(name)==-1) {
		tuitt.push(name)
		console.log(name + ' is successfully added.');
	}
	else  {
		console.log('The name "' + name + '" already exist.')
	}
}
